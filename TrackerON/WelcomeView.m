//
//  WelcomeView.m
//  TrackerON
//
//  Created by srikanth on 3/2/14.
//  Copyright (c) 2014 Sandeep. All rights reserved.
//

#import "WelcomeView.h"
#import "RegistrationView.h"
#import "SignINViewController.h"

@interface WelcomeView ()

@end

@implementation WelcomeView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [btnSignUp setBackgroundColor:[UIColor colorWithRed:0.0f/255.0f green:125.0f/255.0f blue:0.0f/255.0f alpha:1.0f]];
    [btnSignUp.layer setCornerRadius:6.0f];
    [btnSignUp.layer setMasksToBounds:YES];
    
    [btnSignIn setBackgroundColor:[UIColor colorWithRed:0.0f/255.0f green:125.0f/255.0f blue:0.0f/255.0f alpha:1.0f]];
    [btnSignIn.layer setCornerRadius:6.0f];
    [btnSignIn.layer setMasksToBounds:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doSignIN:(UIButton *)sender {
    @try {
        SignINViewController *svc = [[SignINViewController alloc] initWithNibName:@"SignINViewController" bundle:nil];
        [self.navigationController pushViewController:svc animated:YES];
    }
    @catch (NSException *exception) {
        if (exception) {
            NSLog(@"Exception while Navigating SignIN");
        }
    }
    @finally {
        
    }
}

- (IBAction)doSignUp:(UIButton *)sender {
    @try {
        RegistrationView *rv = [[RegistrationView alloc] initWithNibName:@"RegistrationView" bundle:nil];
        [self.navigationController pushViewController:rv animated:YES];
    }
    @catch (NSException *exception) {
        if (exception) {
            NSLog(@"Exception while Navigating Registration");
        }
    }
    @finally {
        
    }
}
@end
