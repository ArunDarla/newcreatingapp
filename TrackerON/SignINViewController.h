//
//  SignINViewController.h
//  TrackerON
//
//  Created by srikanth on 3/2/14.
//  Copyright (c) 2014 Sandeep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface SignINViewController : UIViewController<UITextFieldDelegate>{
    
    IBOutlet UITextField *txtUsername;
    IBOutlet UITextField *txtPassword;
    IBOutlet UIButton *btnCancel;
    IBOutlet UIButton *btnSignIn;
}
- (IBAction)cancel:(id)sender;
- (IBAction)doSignIn:(UIButton *)sender;

@end
