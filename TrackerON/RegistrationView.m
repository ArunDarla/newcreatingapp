//
//  RegistrationView.m
//  TrackerON
//
//  Created by srikanth on 3/2/14.
//  Copyright (c) 2014 Sandeep. All rights reserved.
//

#import "RegistrationView.h"
#import "SignINViewController.h"

@interface RegistrationView ()

@end

@implementation RegistrationView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    btnCance.titleLabel.textColor = [UIColor colorWithRed:0.0f/255.0f green:125.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
    btnSave.titleLabel.textColor = [UIColor colorWithRed:0.0f/255.0f green:125.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)cancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)doSaveInformation:(UIButton *)sender {
    
    if ([txtUsername.text isEqualToString:@""] || txtUsername.text == nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Required!" message:@"Please enter Username" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    if ([txtPassword.text isEqualToString:@""] || txtPassword.text == nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Required!" message:@"Please enter Password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    if ([txtPassword.text length] < 4) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Required!" message:@"Password must be atleast 4 characters" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    if ([txtConfirmPass.text isEqualToString:@""] || txtConfirmPass.text == nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Required!" message:@"Please enter Confirm Password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    if (![txtPassword.text isEqualToString:txtConfirmPass.text]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Not Match!" message:@"Both password doesn't match." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    // Validation Success
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"IsSignUp"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"You have successfully Registered" delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:@"Login", nil];
    [alert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if ([alertView.title isEqualToString:@"Success"]) {
        if (buttonIndex == 1) {
            @try {
                SignINViewController *svc = [[SignINViewController alloc] initWithNibName:@"SignINViewController" bundle:nil];
                [self.navigationController pushViewController:svc animated:YES];
            }
            @catch (NSException *exception) {
                
            }
            @finally {
                
            }
        }
    }
}

@end
