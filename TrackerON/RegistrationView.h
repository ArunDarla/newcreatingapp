//
//  RegistrationView.h
//  TrackerON
//
//  Created by srikanth on 3/2/14.
//  Copyright (c) 2014 Sandeep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistrationView : UIViewController<UITextFieldDelegate, UIAlertViewDelegate>{
    
    IBOutlet UITextField *txtUsername;
    IBOutlet UITextField *txtPassword;
    IBOutlet UITextField *txtConfirmPass;
    IBOutlet UIButton *btnCance;
    IBOutlet UIButton *btnSave;
}
- (IBAction)cancel:(id)sender;
- (IBAction)doSaveInformation:(UIButton *)sender;

@end
