//
//  HomeViewController.h
//  TrackerON
//
//  Created by srikanth on 3/2/14.
//  Copyright (c) 2014 Sandeep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "RelationViewController.h"


@interface HomeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    
}
- (IBAction)doSignout:(id)sender;

@end
