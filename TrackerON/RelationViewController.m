//
//  RelationViewController.m
//  TrackerON
//
//  Created by Venkata Chinni on 3/3/14.
//  Copyright (c) 2014 Sandeep. All rights reserved.
//

#import "RelationViewController.h"

@interface RelationViewController ()

@end

@implementation RelationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
   NSString *getSelectedStr = [[NSUserDefaults standardUserDefaults]objectForKey:@"yourSelectedRowStringKey"];
    NSLog(@"getSelectedStr--%@",getSelectedStr);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
