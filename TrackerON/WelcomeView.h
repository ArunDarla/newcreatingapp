//
//  WelcomeView.h
//  TrackerON
//
//  Created by srikanth on 3/2/14.
//  Copyright (c) 2014 Sandeep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeView : UIViewController{
    
    IBOutlet UILabel *lblWelcome;
    IBOutlet UIButton *btnSignIn;
    IBOutlet UIButton *btnSignUp;
}
- (IBAction)doSignIN:(UIButton *)sender;
- (IBAction)doSignUp:(UIButton *)sender;

@end
