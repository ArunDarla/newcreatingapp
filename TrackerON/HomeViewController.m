//
//  HomeViewController.m
//  TrackerON
//
//  Created by srikanth on 3/2/14.
//  Copyright (c) 2014 Sandeep. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()

@property(strong, nonatomic) IBOutlet UITableView *tabView;
@property(strong, nonatomic) NSMutableArray *array,*array1;

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _array = [NSMutableArray arrayWithObjects:@"Family", nil];
    _array1 = [NSMutableArray arrayWithObjects:@"Friends", nil];
    
    _tabView.delegate=self;
    _tabView.dataSource=self;
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:YES];
}

- (IBAction)doSignout:(id)sender {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Username"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [delegate isSignedIn:NO];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (section == 0) {
       return [_array count];
    }else if(section == 1){
        return [_array1 count];
        
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 80;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    if (indexPath.section == 0) {
        cell.textLabel.text  = [_array objectAtIndex:indexPath.row];
    }else if (indexPath.section == 1){
        cell.textLabel.text  = [_array1 objectAtIndex:indexPath.row];
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSString *yourSelectedRowString;
    if (indexPath.section == 0) {
        yourSelectedRowString=[_array objectAtIndex:indexPath.row];
           }else if (indexPath.section == 1){
       yourSelectedRowString=[_array1 objectAtIndex:indexPath.row];
        
    }
    NSLog(@"yourSelectedRowString--%@",yourSelectedRowString);
   
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove the row from data model
    if (indexPath.section == 0) {
        [_array removeObjectAtIndex:indexPath.row];
    }else if (indexPath.section == 1){
        [_array1 removeObjectAtIndex:indexPath.row];
    }
    
    // Request table view to reload
    [tableView reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
